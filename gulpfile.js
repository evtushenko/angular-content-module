/* global require */
/* global __dirname */
var gulp    = require('gulp');
var uglify  = require('gulp-uglify');
var ngtpl   = require('gulp-angular-templatecache');
var concat  = require('gulp-concat');
var order   = require('gulp-order');

gulp.task('tpl', function () {
    return gulp.src(__dirname + '/src/templates/**/*.html')
        .pipe(ngtpl({
            module: 'component.templates',
            root: 'component',
            standalone: true
        }))
        .pipe(gulp.dest(__dirname + '/src/js'));
});
gulp.task('js', ['tpl'], function () {
    return gulp.src(__dirname + '/src/js/**/*.js')
        .pipe(order([
            'directives.js',
            'module.js'
        ]))
        .pipe(concat('component.js'))
        .pipe(uglify())
        .pipe(gulp.dest(__dirname + '/dist'));
});

gulp.task('build', ['js']);
gulp.task('default', ['build']);