(function() {
    'use strict';

    angular.module('component.directives', [
        'component.templates'
    ])
        .directive('componentExample', function () {
            return {
                templateUrl: 'component/example.html',
                link: function ($scope) {
                    $scope.view = 'view1';
                    $scope.switchToView1 = function () {
                        $scope.view = 'view1';
                    };
                    $scope.switchToView2 = function () {
                        $scope.view = 'view2';
                    };
                }
            };
        });
})();