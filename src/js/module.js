(function() {
    'use strict';

    angular.module('component.module', [
        'component.directives'
    ]);
})();